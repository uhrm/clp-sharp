using NUnit.Framework;

namespace Clp.Tests
{
    [TestFixture]
    public class TestAdlittle : TestLp
    {
        private Clp.Solver CreateSolver()
        {
            var mod = new Clp.Solver();
            mod.LogLevel = 0;
            mod.ReadMps($"{System.AppContext.BaseDirectory}Resources/adlittle.mps");
            return mod;
        }

        [Test]
        public void TestSolve()
        {
            using (var mod = CreateSolver())
            {
                mod.InitialSolve();

                Assert.AreEqual(ProblemStatus.Optimal, mod.Status);

                // this is from the NETLIB readme
                Assert.AreEqual(2.2549496316e5, mod.ObjectiveValue, 1e-5);

                CheckKKT(mod, 1e-10);
            }
        }
    }
}
