using NUnit.Framework;

namespace Clp.Tests
{
    [TestFixture]
    public class TestAfiro : TestLp
    {
        
        private Clp.Solver CreateSolver()
        {
            var  mod = new Clp.Solver();
            mod.LogLevel = 0;
            mod.ReadMps($"{System.AppContext.BaseDirectory}Resources/afiro.mps");
            return mod;
        }

        [Test]
        public void TestDimensions()
        {
            using (var mod = CreateSolver())
            {
                Assert.AreEqual(27, mod.NumRows);
                Assert.AreEqual(32, mod.NumColumns);
            }
        }

        [Test]
        public void TestSolve()
        {
            using (var mod = CreateSolver())
            {
                mod.InitialSolve();

                Assert.AreEqual(ProblemStatus.Optimal, mod.Status);

                // this is from the NETLIB readme
                Assert.AreEqual(-4.6475314286e2, mod.ObjectiveValue, 1e-8);

                CheckKKT(mod, 1e-10);
            }
        }
    }
}
