using System;
using NUnit.Framework;

namespace Clp.Tests
{
    //   Minimize:
    // 
    //       x1 + x2 + x3
    // 
    //   Subject to:
    // 
    //       10 <= x1     + x2 
    //             x1              + x3 <=  4
    //        4 <=          x2     + x3 <=  5
    // 
    //   With:
    // 
    //     0 <= x1
    //     0 <= x2
    //     0 <= x3 <= 3.

    [TestFixture]
    public class TestInfeasibleProgram : TestLp
    {
        private const double infty = Double.PositiveInfinity;

        private Clp.Solver CreateSolver()
        {
            var mod = new Clp.Solver();
            mod.LoadProblem(3, 3,
                new int[] { 0, 2, 4, 6 },
                new int[] { 0, 1, 0, 2, 1, 2 },
                new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 },
                new double[] { 0.0, 0.0, 0.0 },
                new double[] { infty, infty, 3.0 },
                new double[] { 1.0, 1.0, 1.0 },
                new double[] { 10.0, -infty, 4.0},
                new double[] { infty, 4.0, 5.0 });
            mod.LogLevel = 0;
            return mod;
        }
        
        [Test]
        public void TestCreate()
        {
            using var mod = CreateSolver();

            Assert.AreEqual(3, mod.NumRows);
            Assert.AreEqual(3, mod.NumColumns);

            Assert.AreEqual(1.0, mod.Objective[0], 1e-14);
            Assert.AreEqual(1.0, mod.Objective[1], 1e-14);
            Assert.AreEqual(1.0, mod.Objective[2], 1e-14);

            Assert.AreEqual( 0.0, mod.ColumnLower[0], 1e-14);
            Assert.AreEqual( 0.0, mod.ColumnLower[1], 1e-14);
            Assert.AreEqual( 0.0, mod.ColumnLower[2], 1e-14);
            Assert.IsTrue(mod.ColumnUpper[0] > 1e20);
            Assert.IsTrue(mod.ColumnUpper[1] > 1e20);
            Assert.AreEqual( 3.0, mod.ColumnUpper[2], 1e-14);

            // Assert.AreEqual( 1.0, mod.GetCoeff(0,0), 1e-10);
            // Assert.AreEqual( 1.0, mod.GetCoeff(1,0), 1e-10);
            // Assert.AreEqual( 0.0, mod.GetCoeff(2,0), 1e-10);

            // Assert.AreEqual( 1.0, mod.GetCoeff(0,1), 1e-10);
            // Assert.AreEqual( 0.0, mod.GetCoeff(1,1), 1e-10);
            // Assert.AreEqual( 1.0, mod.GetCoeff(2,1), 1e-10);

            // Assert.AreEqual( 0.0, mod.GetCoeff(0,2), 1e-10);
            // Assert.AreEqual( 1.0, mod.GetCoeff(1,2), 1e-10);
            // Assert.AreEqual( 1.0, mod.GetCoeff(2,2), 1e-10);

            Assert.AreEqual(10.0, mod.RowLower[0], 1e-10);
            Assert.IsTrue(mod.RowLower[1] < -1e20);
            Assert.AreEqual( 4.0, mod.RowLower[2], 1e-10);
            Assert.IsTrue(mod.RowUpper[0] >  1e20);
            Assert.AreEqual( 4.0, mod.RowUpper[1], 1e-10);
            Assert.AreEqual( 5.0, mod.RowUpper[2], 1e-10);
        }
        
        [Test]
        public void TestSolve()
        {
            using var mod = CreateSolver();

            mod.InitialSolve();

            Assert.AreEqual(mod.Status, ProblemStatus.PrimalInfeasible);
            // CheckKKT();
        }
        
        [Test]
        public void TestKKT()
        {
            using var mod = CreateSolver();

            mod.InitialSolve();

            var infray = new double[mod.NumRows];
            mod.GetInfeasibilityRay(infray);

            // ***DEBUG***
            // Console.WriteLine();
            // for (var i=0; i<mod.NumConstrs; i++)
            // {
            //     Console.WriteLine("*** pi[{0}] = {1,7:0.0000}", i, mod.Pi[i]);
            // }
            // for (var i=0; i<mod.NumVars; i++)
            // {
            //     Console.WriteLine("*** rc[{0}] = {1,7:0.0000} ({2})", i, mod.Rc[i], mod.VBasis[i]);
            // }
            // for (var i=0; i<mod.NumConstrs; i++)
            // {
            //     Console.WriteLine("*** fd[{0}] = {1,7:0.0000}", i, mod.FarkasDual[i]);
            // }
            // ***ENDEBUG***

            // check dual feasibility
            for (var i=0; i<mod.NumRows; i++) {
                var pi = mod.DualRowSolution[i];
                switch (mod.RowStatus[i])
                {
                case Status.Free:
                    Assert.AreEqual(0.0, pi, 1e-8, String.Format("Dual of free constraint {0} is nonzero: {1}.", i, pi));
                    break;
                case Status.Basic:
                    Assert.AreEqual(0.0, pi, 1e-8, String.Format("Dual of basic constraint {0} is nonzero: {1}.", i, pi));
                    break;
                case Status.AtUpper:
                    Assert.IsTrue(pi <  1e-8, String.Format("Dual of constraint {0} at upper bound is positive: {1}.", i, pi));
                    break;
                case Status.AtLower:
                    Assert.IsTrue(pi > -1e-8, String.Format("Dual of constraint {0} at lower bound is negative: {1}.", i, pi));
                    break;
                case Status.Fixed:
                    // no constraint on pi
                    break;
                default:
                    Assert.Fail(String.Format("Unexpected row status of constraint {0}: {1}.", i, mod.RowStatus[i]));
                    break;
                }
            }
            for (var j=0; j<mod.NumColumns; j++) {
                var rc = mod.DualColumnSolution[j];
                switch (mod.ColumnStatus[j])
                {
                case Status.Basic:
                    Assert.IsTrue(Math.Abs(rc) < 1e-8, String.Format("Reduced cost of basic column {0} is non-zero: {1}", j, rc));
                    break;
                case Status.AtUpper:
                    Assert.IsTrue(rc <  1e-8, String.Format("Reduced cost of column {0} at upper bound is positive: {1}", j, rc));
                    break;
                case Status.AtLower:
                    Assert.IsTrue(rc > -1e-8, String.Format("Reduced cost of column {0} at lower bound is negative: {1}", j, rc));
                    break;
                default:
                    Assert.Fail(String.Format("Unexpected status of column {0}: {1}.", j, mod.ColumnStatus[j]));
                    break;
                }
            }

            // check infeasibility ray
            var infval = 0.0;
            for (var i=0; i<mod.NumRows; i++) {
                var ri = -infray[i];
                switch (mod.RowStatus[i])
                {
                case Status.Free:
                    Assert.AreEqual(0.0, ri, 1e-8, String.Format("Infeasibility ray of free constraint {0} is nonzero: {1}.", i, ri));
                    break;
                case Status.Basic:
                case Status.Fixed:
                    if (ri < 0.0) {
                        infval += ri*mod.RowUpper[i];
                    }
                    else {
                        infval += ri*mod.RowLower[i];
                    }
                    break;
                case Status.AtUpper:
                    Assert.IsTrue(ri <  1e-8, String.Format("Infeasibility ray of constraint {0} at upper bound is positive: {1}.", i, ri));
                    infval += ri*mod.RowUpper[i];
                    break;
                case Status.AtLower:
                    Assert.IsTrue(ri > -1e-8, String.Format("Infeasibility ray of constraint {0} at lower bound is negative: {1}.", i, ri));
                    infval += ri*mod.RowLower[i];
                    break;
                default:
                    Assert.Fail(String.Format("Unexpected status of row {0}: {1}.", i, mod.RowStatus[i]));
                    break;
                }
            }
            var Ap = new int[3];
            var Al = new int[3];
            var Ai = new int[6];
            var Ax = new double[6];
            mod.VectorStarts.CopyTo(Ap);
            mod.VectorLengths.CopyTo(Al);
            mod.Indices.CopyTo(Ai);
            mod.Elements.CopyTo(Ax);
            for (var j=0; j<mod.NumColumns; j++) {
                var res = 0.0;
                for (var k=Ap[j]; k<Ap[j]+Al[j]; k++) {
                    var i = Ai[k];
                    var a = Ax[k];
                    var ri = -infray[i];
                    res += ri*a;
                }
                if (res > 1e-8) {
                    // complete unbd ray with dual variable of upper bound j
                    Assert.AreEqual(Status.AtUpper, mod.ColumnStatus[j]);
                    //Assert.AreEqual(mod.Rc[j], -res, 1e-8);
                    infval -= res*mod.ColumnUpper[j];
                }
                else if (res < -1e-8) {
                    // complete unbd ray with dual variable of lower bound j
                    Assert.AreEqual(Status.AtLower, mod.ColumnStatus[j]);
                    infval -= res*mod.ColumnLower[j];
                }
                Assert.IsTrue(infval >= 1e-8, String.Format("Nonpositive obj val of infeas ray: {0}.", infval));
            }
        }
    }
}

