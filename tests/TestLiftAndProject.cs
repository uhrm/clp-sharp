using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Clp.Tests
{
    //   P1:
    // 
    //         2 * x1  - 4 * x2  >=  -3
    //        -4 * x1  - 2 * x2  >=  -5
    // 
    //   P2:
    // 
    //         4 * x1  - 4 * x2  >=  -1
    //        -4 * x1  + 4 * x2  >=  -1
    // 
    //   With:
    // 
    //     0 <= x1 <= 1
    //     0 <= x2 <= 1

    [TestFixture]
    public class TestLiftAndProject : TestLp
    {
        private const double infty = Double.PositiveInfinity;

        private class CutData
        {
            public readonly IReadOnlyList<double> x;
            public readonly int pvt;

            public readonly Clp.Solver mod;
            public readonly Clp.Solver sep;

            public readonly IReadOnlyList<int> lbinfo;
            public readonly IReadOnlyList<int> ubinfo;

            public CutData(IReadOnlyList<double> x, int pvt, Clp.Solver mod, Clp.Solver sep, IReadOnlyList<int> lb, IReadOnlyList<int> ub)
            {
                this.x = x;
                this.pvt = pvt;
                this.mod = mod;
                this.sep = sep;
                this.lbinfo = lb;
                this.ubinfo = ub;
            }

            public int m { get { return mod.NumRows; } }

            public int n { get { return mod.NumColumns; } }
        }

        private CutData BuildSep(Clp.Solver mod, Clp.Solver sep, double[] x, int pvt)
        {
            var m = mod.NumRows;
            var n = mod.NumColumns;

            // compute row activities
            var a = new double[m];
            for (var j = 0; j < n; j++) {
                var jofs = mod.VectorStarts[j];
                var jlen = mod.VectorLengths[j];
                for (var k = jofs; k < jofs+jlen; k++) {
                    var i = mod.Indices[k];
                    var aij = mod.Elements[k];
                    a[i] += aij * x[j];
                }
            }

            var bhat = new double[m];
            for (var i = 0; i < m; i++) {
                bhat[i] = mod.RowLower[i]*x[pvt];
            }

            var rhat = new double[m];
            for (var i = 0; i < m; i++) {
                rhat[i] = a[i] - mod.RowLower[i];
            }

            // bound info
            var lbinfo = new int[n];
            var ubinfo = new int[n];

            // build separation problem
            Clp.Solver.Copy(mod, sep);
            for (var i = 0; i < m; i++) {
                sep.RowLower[i] = bhat[i];
                sep.RowUpper[i] = bhat[i] + rhat[i];
            }
            for (var j = 0; j < n; j++) {
                var lj = mod.ColumnLower[j];
                var uj = mod.ColumnUpper[j];
                var lhat = lj*x[pvt];
                var uhat = uj*x[pvt];
                // lower bound
                var lu = uhat - (uj - x[j]);
                if (lhat > lu) {
                    sep.ColumnLower[j] = lhat;
                    lbinfo[j] = 0;
                }
                else {
                    sep.ColumnLower[j] = lu;
                    lbinfo[j] = 1;
                }
                // upper bound
                var ul = lhat + (x[j] - lj);
                if (uhat < ul) {
                    sep.ColumnUpper[j] = uhat;
                    ubinfo[j] = 0;
                }
                else {
                    sep.ColumnUpper[j] = ul;
                    ubinfo[j] = 1;
                }
                sep.Objective[j] = j == pvt ? -1.0 : 0.0;
            }

            return new CutData(x, pvt, mod, sep, lbinfo, ubinfo);
        }

        private void CheckSep(Clp.Solver sep, int pvt)
        {
            var dval = 0.0;
            for (var i=0; i<sep.NumRows; i++) {
                switch (sep.RowStatus[i]) {
                case Status.AtUpper: {
                    var rub = sep.RowUpper[i];
                    var pi = sep.DualRowSolution[i];
                    Assert.IsTrue(pi <= 0.0, String.Format("Pi expected non-negative, but found pi = {0}.", pi));
                    dval += pi * rub;
                    break;
                }
                case Status.AtLower: {
                    var rlb = sep.RowLower[i];
                    var pi = sep.DualRowSolution[i];
                    Assert.IsTrue(pi >= 0.0, String.Format("Pi expected non-positive, but found pi = {0}.", pi));
                    dval += pi * rlb;
                    break;
                }
                case Status.Fixed: {
                    var rhs = sep.RowLower[i];
                    var pi = sep.DualRowSolution[i];
                    dval += pi * rhs;
                    break;
                }}
            }
            for (var j = 0; j < sep.NumColumns; j++) {
                switch (sep.ColumnStatus[j]) {
                case Status.AtUpper: {
                    var ub = sep.ColumnUpper[j];
                    var rc = sep.DualColumnSolution[j];
                    Assert.IsTrue(rc <= 0.0);
                    dval += rc * ub;
                    break;
                }
                case Status.AtLower: {
                    var lb = sep.ColumnLower[j];
                    var rc = sep.DualColumnSolution[j];
                    Assert.IsTrue(rc >= 0.0);
                    dval += rc * lb;
                    break;
                }
                case Status.Fixed: {
                    var lb = sep.ColumnLower[j];
                    var rc = sep.DualColumnSolution[j];
                    dval += rc * lb;
                    break;
                }}
            }
            Assert.AreEqual(sep.ObjectiveValue, dval, 1e-10);
        }

        private void PrintCut(CutData cut/*Clp.Solver mod, Clp.Solver sep, int pvt*/)
        {
            var m = cut.m;
            var n = cut.n;
            var pvt = cut.pvt;
            var mod = cut.mod;
            var sep = cut.sep;

            // cut coefs
            var d = new double[n];
            for (var j=0; j<n; j++) {
                if (cut.lbinfo[j] == 1) {
                    d[j] += -Math.Max(sep.DualColumnSolution[j], 0.0);
                }
                if (cut.ubinfo[j] == 1) {
                    d[j] += -Math.Min(sep.DualColumnSolution[j], 0.0);
                }
                var jofs = mod.VectorStarts[j];
                var jlen = mod.VectorLengths[j];
                for (var k=jofs; k<jofs+jlen; k++) {
                    var i = mod.Indices[k];
                    var p = Math.Min(sep.DualRowSolution[i], 0.0);
                    d[j] += -p*mod.Elements[k];
                }
            }
            for (var i=0; i<m; i++) {
                var p = sep.DualRowSolution[i];
                d[pvt] += -p*mod.RowLower[i];
            }
            for (var k=0; k<n; k++) {
                if (cut.lbinfo[k] == 1) {
                    var uk = mod.ColumnUpper[k];
                    d[pvt] += -Math.Max(sep.DualColumnSolution[k], 0.0)*uk;
                }
                else {
                    var lk = mod.ColumnLower[k];
                    d[pvt] += -Math.Max(sep.DualColumnSolution[k], 0.0)*lk;
                }
                if (cut.ubinfo[k] == 1) {
                    var lk = mod.ColumnLower[k];
                    d[pvt] += -Math.Min(sep.DualColumnSolution[k], 0.0)*lk;
                }
                else {
                    var uk = mod.ColumnUpper[k];
                    d[pvt] += -Math.Min(sep.DualColumnSolution[k], 0.0)*uk;
                }
            }
            d[pvt] -= 1.0;
            // cut rhs
            var e = 0.0;
            for (var i=0; i<m; i++) {
                var p = Math.Min(sep.DualRowSolution[i], 0.0);
                e += -p*mod.RowLower[i];
            }
            for (var j=0; j<n; j++) {
                if (cut.lbinfo[j] == 1) {
                    var uj = mod.ColumnUpper[j];
                    e += -Math.Max(sep.DualColumnSolution[j], 0.0)*uj;
                }
                if (cut.ubinfo[j] == 1) {
                    var lj = mod.ColumnLower[j];
                    e += -Math.Min(sep.DualColumnSolution[j], 0.0)*lj;
                }
            }

            Console.Write("*** sep cut:");
            for (var j=0; j<n; j++) {
                if (j > 0) {
                    Console.Write("  +");
                }
                Console.Write("  {0} * x[{1}]", d[j], j);
            }
            Console.WriteLine("  >=  {0}", e);
        }

        [Test]
        public void TestP1P2C()
        {
            using (var mod = new Clp.Solver())
            using (var sep = new Clp.Solver())
            {
                mod.LogLevel = 0;
                sep.LogLevel = 0;

                mod.LoadProblem(
                    2, 4,
                    new int[] { 0, 4, 8 },
                    new int[] { 0, 1, 2, 3, 0, 1, 2, 3 },
                    new double[] { 2.0, -4.0, 4.0, -4.0, -4.0, -2.0, -4.0, 4.0 },
                    new double[] { 0.0, 0.0 },
                    new double[] { 1.0, 1.0 },
                    new double[] { 0.0, -1.0 },
                    new double[] { -3.0, -5.0, -1.0, -1.0 },
                    new double[] { infty, infty, infty, infty });

                mod.InitialSolve();

                Assert.AreEqual(mod.Status, ProblemStatus.Optimal);

                Assert.AreEqual(-1.0, mod.ObjectiveValue, 1e-12);

                Assert.AreEqual(0.75, mod.PrimalColumnSolution[0], 1e-12);
                Assert.AreEqual(1.00, mod.PrimalColumnSolution[1], 1e-12);

                CheckKKT(mod, 1e-12);

                var x = new double[2];
                mod.PrimalColumnSolution.CopyTo(x);

                var pvt = 0;

                // build separation problem
                var cut = BuildSep(mod, sep, x, pvt);

                //for (var j=0; j<2; j++) {
                //	Console.WriteLine("*** l[{0}] = {1}", j, sep.ColumnLower[j]);
                //}
                //Console.Out.Flush();

                // solve separation problem
                sep.InitialSolve();

                //				for (var i = 0; i < 4; i++) {
                //					Console.WriteLine("*** pi[{0}] = {1}", i, sep.DualRowSolution[i]);
                //				}
                //				for (var j = 0; j < 2; j++) {
                //					Console.WriteLine("*** rc[{0}] = {1}", j, sep.DualColumnSolution[j]);
                //				}

                Assert.AreEqual(sep.Status, ProblemStatus.Optimal);
                Assert.IsTrue(sep.ObjectiveValue > -x[pvt] + 1e-8);

                CheckKKT(sep, 1e-12);
                CheckSep(sep, pvt);

                // PrintCut(cut/*mod, sep, pvt*/);
            }
        }

        [Test]
        public void TestP1C()
        {
            using (var mod = new Clp.Solver())
            using (var sep = new Clp.Solver())
            {
                mod.LogLevel = 0;
                sep.LogLevel = 0;

                mod.LoadProblem(
                    2, 2,
                    new int[] { 0, 2, 4 },
                    new int[] { 0, 1, 0, 1 },
                    new double[] { 2.0, -4.0, -4.0, -2.0 },
                    new double[] { 0.0, 0.0 },
                    new double[] { 1.0, 1.0 },
                    new double[] { 0.0, -1.0 },
                    new double[] { -3.0, -5.0 },
                    new double[] { infty, infty });

                mod.InitialSolve();

                Assert.AreEqual(mod.Status, ProblemStatus.Optimal);

                Assert.AreEqual(-1.0, mod.ObjectiveValue, 1e-12);

                //Assert.AreEqual( 0.75, mod.PrimalColumnSolution[0], 1e-12);
                Assert.AreEqual(1.00, mod.PrimalColumnSolution[1], 1e-12);

                CheckKKT(mod, 1e-12);

                var x = new double[2];
                mod.PrimalColumnSolution.CopyTo(x);
                //for (var j = 0; j < 2; j++) {
                //	Console.WriteLine("*** x[{0}] = {1}", j, x[j]);
                //}

                var pvt = 0;

                // build separation problem
                var cut = BuildSep(mod, sep, x, pvt);

                // solve separation problem
                sep.InitialSolve();

                Assert.AreEqual(sep.Status, ProblemStatus.Optimal);
                Assert.IsTrue(sep.ObjectiveValue > -x[pvt] + 1e-8);
                //Console.WriteLine("*** sep obj: {0}", sep.ObjectiveValue);

                CheckKKT(sep, 1e-12);
                CheckSep(sep, pvt);

                // PrintCut(cut/*mod, sep, pvt*/);
            }
        }

        [Test]
        public void TestP1()
        {
            using (var mod = new Clp.Solver())
            using (var sep = new Clp.Solver())
            {
                mod.LogLevel = 0;
                sep.LogLevel = 0;

                mod.LoadProblem(
                    2, 2,
                    new int[] { 0, 2, 4 },
                    new int[] { 0, 1, 0, 1 },
                    new double[] { 2.0, -4.0, -4.0, -2.0 },
                    new double[] { -infty, -infty },
                    new double[] { infty, infty },
                    new double[] { 0.0, -1.0 },
                    new double[] { -3.0, -5.0 },
                    new double[] { infty, infty });

                var x = new double[] { 0.75, 0.75 };
                //var x = new double[] { 0.5, 1.0 };
                //var x = new double[] { 0.75, 1.0 };
                //for (var j = 0; j < 2; j++) {
                //	Console.WriteLine("*** x[{0}] = {1}", j, x[j]);
                //}

                var pvt = 0;

                // build separation problem
                var cut = BuildSep(mod, sep, x, pvt);

                // solve separation problem
                sep.InitialSolve();

                Assert.AreEqual(sep.Status, ProblemStatus.Optimal);
                Assert.IsTrue(sep.ObjectiveValue > -x[pvt] + 1e-8);
                //Console.WriteLine("*** sep obj: {0}", sep.ObjectiveValue);

                CheckKKT(sep, 1e-12);
                CheckSep(sep, pvt);

                // PrintCut(cut/*mod, sep, pvt*/);
            }
        }
    }
}
