﻿using System;
using NUnit.Framework;

namespace Clp.Tests
{
    [TestFixture]
    public class TestSimpleProgram
    {
//        private Clp.Solver mod;
//
//        [SetUp]
//        public void SetUp()
//        {
//            mod = new Clp.Solver();
//            
//            mod.LogLevel = 0;
//        }
//
//        [TearDown]
//        public void Dispose()
//        {
//            mod.Dispose();
//        }

        private void CheckProblemData(Clp.Solver mod)
        {
            // problem name
            Assert.AreEqual("simple_program", mod.ProblemName);

            // problem dimensions
            Assert.AreEqual(2, mod.NumRows);
            Assert.AreEqual(2, mod.NumColumns);

            // objective
            double[] obj = new double[2];
            mod.Objective.CopyTo(obj);
            Assert.AreEqual(-1.0, obj[0], 1e-10);
            Assert.AreEqual(-5.0, obj[1], 1e-10);
            Assert.AreEqual(-1.0, mod.Objective[0], 1e-10);
            Assert.AreEqual(-5.0, mod.Objective[1], 1e-10);

            // row lower bounds
            double[] rlb = new double[2];
            mod.RowLower.CopyTo(rlb);
            Assert.IsTrue(rlb[0] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 0, rlb[0]));
            Assert.IsTrue(rlb[1] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 1, rlb[1]));
            Assert.IsTrue(mod.RowLower[0] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 0, mod.RowLower[0]));
            Assert.IsTrue(mod.RowLower[1] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 1, mod.RowLower[1]));

            // row upper bounds
            double[] rub = new double[2];
            mod.RowUpper.CopyTo(rub);
            Assert.AreEqual( 99.0, rub[0], 1e-14);
            Assert.AreEqual(165.0, rub[1], 1e-14);
            Assert.AreEqual( 99.0, mod.RowUpper[0], 1e-14);
            Assert.AreEqual(165.0, mod.RowUpper[1], 1e-14);

            // column lower bounds
            double[] lb = new double[2];
            mod.ColumnLower.CopyTo(lb);
            Assert.IsTrue(lb[0] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 0, lb[0]));
            Assert.IsTrue(lb[1] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 1, lb[1]));
            Assert.IsTrue(mod.ColumnLower[0] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 0, mod.ColumnLower[0]));
            Assert.IsTrue(mod.ColumnLower[1] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 1, mod.ColumnLower[1]));

            // column upper bounds
            var ub = new double[2];
            mod.ColumnUpper.CopyTo(ub);
            Assert.IsTrue(ub[0] > 1e20, String.Format("Invalid upper bound of row {0}: expected value > 1e20, found {1}.", 0, ub[0]));
            Assert.IsTrue(ub[1] > 1e20, String.Format("Invalid upper bound of row {0}: expected value > 1e20, found {1}.", 1, ub[1]));
            Assert.IsTrue(mod.ColumnUpper[0] > 1e20, String.Format("Invalid upper bound of row {0}: expected value > 1e20, found {1}.", 0, mod.ColumnUpper[0]));
            Assert.IsTrue(mod.ColumnUpper[1] > 1e20, String.Format("Invalid upper bound of row {0}: expected value > 1e20, found {1}.", 1, mod.ColumnUpper[1]));

            // constraint matrix
            Assert.AreEqual(4, mod.NumElements);
            var Ap = new int[2];
            var Al = new int[2];
            var Ai = new int[4];
            var Ax = new double[4];
            mod.VectorStarts.CopyTo(Ap);
            mod.VectorLengths.CopyTo(Al);
            mod.Indices.CopyTo(Ai);
            mod.Elements.CopyTo(Ax);
            var v = new double[] { -2.0, 11.0, 11.0, 4.0 };
            var nnz = 0;
            for (var j=0; j<2; j++) {
                Assert.AreEqual(2*j, Ap[j]);
                for (var k=Ap[j]; k<Ap[j]+Al[j]; k++) {
                    var i = Ai[k];
                    var x = Ax[k];
                    Assert.AreEqual(v[2*j+i], x, 1e-14, String.Format("Invalid matrix element at ({0},{1}): expected {2}, found {3}.", i, j, v[2*j+i], x));
                }
                nnz += Al[j];
            }
           Assert.AreEqual(4, nnz);

            // row names
            Assert.AreEqual("R1", mod.RowName[0]);
            Assert.AreEqual(0, mod.RowName[1].Length);
            // column names
            Assert.AreEqual("C1", mod.ColumnName[0]);
            Assert.AreEqual(0, mod.ColumnName[1].Length);
        }

       private void CheckSolution(Clp.Solver mod)
        {
            mod.InitialSolve();

            Assert.AreEqual(-66.0, mod.ObjectiveValue, 1e-9);

            var x = new double[2];
            mod.PrimalColumnSolution.CopyTo(x);
            Assert.AreEqual(11.0, x[0], 1e-9);
            Assert.AreEqual(11.0, x[1], 1e-9);

            Assert.AreEqual(11.0, mod.PrimalColumnSolution[0], 1e-9);
            Assert.AreEqual(11.0, mod.PrimalColumnSolution[1], 1e-9);
        }

        [Test]
        public void TestLoad()
        {
            using (var mod = new Clp.Solver())
            {
                mod.ProblemName = "simple_program";
                mod.LogLevel = 0;

                // load problem data
                mod.LoadProblem(2, 2,
                    new int[] { 0, 2, 4 },
                    new int[] { 0, 1, 0, 1 },
                    new double[] { -2.0, 11.0, 11.0, 4.0 },
                    new double[] { Double.NegativeInfinity, Double.NegativeInfinity },
                    new double[] { Double.PositiveInfinity, Double.PositiveInfinity },
                    new double[] { -1.0, -5.0 },
                    new double[] { Double.NegativeInfinity, Double.NegativeInfinity },
                    new double[] { 99.0, 165.0 });
                mod.RowName[0] = "R1";
                mod.RowName[1] = "";
                mod.ColumnName[0] = "C1";
                // mod.ColumnName[1] = null;

                CheckProblemData(mod);

                CheckSolution(mod);
            }
        }

        [Test]
        public void TestRowsCols()
        {
            using (var mod = new Clp.Solver())
            {
                mod.ProblemName = "simple_program";
                mod.LogLevel = 0;

                // add problem rows
                mod.Resize(2, 0);
                mod.RowLower[0] = Double.NegativeInfinity;
                mod.RowLower[1] = Double.NegativeInfinity;
                mod.RowUpper[0] = 99.0;
                mod.RowUpper[1] = 165.0;

                // add problem columns
                mod.AddColumns(
                    new double[] { Double.NegativeInfinity, Double.NegativeInfinity },
                    new double[] { Double.PositiveInfinity, Double.PositiveInfinity },
                    new double[] { -1.0, -5.0 },
                    new int[] { 0, 2, 4 },
                    new int[] { 0, 1, 0, 1 },
                    new double[] { -2.0, 11.0, 11.0, 4.0 });
                mod.RowName[0] = "R1";
                mod.RowName[1] = "";
                mod.ColumnName[0] = "C1";
                // mod.ColumnName[1] = null;

                CheckProblemData(mod);

                CheckSolution(mod);
            }
        }

        [Test]
        public void TestCopy()
        {
            using (var mod = new Clp.Solver())
            using (var cpy = new Clp.Solver())
            {
                mod.ProblemName = "simple_program";
                mod.LogLevel = 0;
                cpy.LogLevel = 0;

                // load problem data
                mod.LoadProblem(2, 2,
                    new int[] { 0, 2, 4 },
                    new int[] { 0, 1, 0, 1 },
                    new double[] { -2.0, 11.0, 11.0, 4.0 },
                    new double[] { Double.NegativeInfinity, Double.NegativeInfinity },
                    new double[] { Double.PositiveInfinity, Double.PositiveInfinity },
                    new double[] { -1.0, -5.0 },
                    new double[] { Double.NegativeInfinity, Double.NegativeInfinity },
                    new double[] { 99.0, 165.0 });
                mod.RowName[0] = "R1";
                mod.RowName[1] = "";
                mod.ColumnName[0] = "C1";
                // mod.ColumnName[1] = null;

                Clp.Solver.Copy(mod, cpy);

                CheckProblemData(cpy);

                CheckSolution(cpy);
            }
        }
    }
}
