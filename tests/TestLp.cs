using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using NUnit.Framework;

namespace Clp.Tests
{
    public abstract class TestLp
    {
        protected void CheckKKT(Clp.Solver solver, double tol)
        {
            CheckKKT(solver, tol, 1e-8);
        }
        
        protected void CheckKKT(Clp.Solver solver, double opttol, double zerotol)
        {
            if (solver.Status == ProblemStatus.Optimal)
            {
                // check dual feasibility
                for (var i=0; i<solver.NumRows; i++)
                {
                    var pi = solver.DualRowSolution[i];
                    switch (solver.RowStatus[i])
                    {
                    // case Status.Free:
                    //     Assert.AreEqual(0.0, pi, 1e-8, String.Format("Dual of free constraint {0} is nonzero: {1}.", i, pi));
                    //     break;
                    case Status.Basic:
                        Assert.AreEqual(0.0, pi, zerotol, $"Dual of basic constraint {i} is nonzero: {pi}.");
                        break;
                    case Status.AtUpper:
                        Assert.IsTrue(pi <  zerotol, $"Dual multiplier of constraint {i} at upper bound is positive: {pi}.");
                        break;
                    case Status.AtLower:
                        Assert.IsTrue(pi > -zerotol, $"Dual multiplier of constraint {i} at lower bound is negative: {pi}.");
                        break;
                    case Status.Fixed:
                        // no constraint on pi
                        break;
                    default:
                        Assert.Fail($"Unexpected row status of constraint {i}: {solver.RowStatus[i]} (dual multiplier: {pi}).");
                        break;
                    }
                }
                for (var j=0; j<solver.NumColumns; j++)
                {
                    var rc = solver.DualColumnSolution[j];
                    switch (solver.ColumnStatus[j])
                    {
                    case Status.Basic:
                        Assert.IsTrue(Math.Abs(rc) < zerotol, $"Reduced cost of basic column {j} is non-zero: {rc}");
                        break;
                    case Status.AtUpper:
                        Assert.IsTrue(rc <  zerotol, $"Reduced cost of column {j} at upper bound is positive: {rc}");
                        break;
                    case Status.AtLower:
                        Assert.IsTrue(rc > -zerotol, $"Reduced cost of column {j} at lower bound is negative: {rc}");
                        break;
                    case Status.Fixed:
                        break;
                    default:
                        Assert.Fail($"Unexpected status of column {j}: {solver.ColumnStatus[j]} (reduced cost: {rc}).");
                        break;
                    }
                }
            }

            // check dual objective
            var dval = 0.0;
            for (var i=0; i<solver.NumRows; i++)
            {
                switch (solver.RowStatus[i]) {
                case Status.AtUpper: {
                    var rub = solver.RowUpper[i];
                    var pi = solver.DualRowSolution[i];
					Assert.IsTrue(pi <= zerotol, $"Dual multiplier {i} expected non-positive, but found pi = {pi}.");
                    dval += pi * rub;
                    break;
                }
                case Status.AtLower: {
                    var rlb = solver.RowLower[i];
                    var pi = solver.DualRowSolution[i];
					Assert.IsTrue(pi >= -zerotol, $"Dual multiplier {i} expected non-negative, but found pi = {pi}.");
                    dval += pi * rlb;
                    break;
                }
                case Status.Fixed: {
                    var rlb = solver.RowLower[i];
                    var pi = solver.DualRowSolution[i];
                    dval += pi * rlb;
                    break;
                }}
            }
            for (var j=0; j<solver.NumColumns; j++)
            {
                switch (solver.ColumnStatus[j]) {
                case Status.AtUpper: {
                    var ub = solver.ColumnUpper[j];
                    var rc = solver.DualColumnSolution[j];
                    Assert.IsTrue(rc <= zerotol, $"Reduced cost of variable {j} expected non-positive, but found rc = {rc}.");
                    dval += rc * ub;
                    break;
                }
                case Status.AtLower: {
                    var lb = solver.ColumnLower[j];
                    var rc = solver.DualColumnSolution[j];
					Assert.IsTrue(rc >= -zerotol, $"Reduced cost of variable {j} expected non-negative, but found rc = {rc}.");
                    dval += rc * lb;
                    break;
                }
                case Status.Fixed: {
                    var lb = solver.ColumnLower[j];
                    var rc = solver.DualColumnSolution[j];
                    dval += rc * lb;
                    break;
                }}
            }
            Assert.AreEqual(solver.ObjectiveValue, dval, opttol);

            // check complementary slackness
            for (var i=0; i<solver.NumRows; i++)
            {
                var aix = solver.PrimalRowSolution[i];
                switch (solver.RowStatus[i]) {
//                case Status.Free: {
                case Status.Basic: {
                    var pi = solver.DualRowSolution[i];
                    Assert.AreEqual(0.0, pi, 1e-8, $"Unexpected dual value for 'basic' constraint {i}: expected pi = 0.0, but found pi = {pi}.");
                    break;
                }
                case Status.AtUpper: {
                    var rub = solver.RowUpper[i];
                    Assert.AreEqual(0.0, rub-aix, 1e-8, $"Unexpected primal value for 'at upper' constraint {i}: expected ai*x = {rub}, but found ai*x = {aix}.");
                    break;
                }
                case Status.AtLower: {
                    var rlb = solver.RowLower[i];
                    Assert.AreEqual(0.0, rlb-aix, 1e-8, $"Unexpected primal value for 'at lower' constraint {i}: expected ai*x = {rlb}, but found ai*x = {aix}.");
                    break;
                }
                case Status.Fixed: {
                    var rub = solver.RowUpper[i];
                    var rlb = solver.RowLower[i];
                    Assert.AreEqual(0.0, rub-aix, 1e-8, $"Unexpected primal value for 'fixed' constraint {i}: expected ai*x = {rub}, but found ai*x = {aix}.");
                    Assert.AreEqual(0.0, rlb-aix, 1e-8, $"Unexpected primal value for 'fixed' constraint {i}: expected ai*x = {rlb}, but found ai*x = {aix}.");
                    break;
                }}
            }
            for (var j=0; j<solver.NumColumns; j++)
            {
                var xj = solver.PrimalColumnSolution[j];
                switch (solver.ColumnStatus[j]) {
                case Status.Basic:
                    var rc = solver.DualColumnSolution[j];
                    Assert.AreEqual(0.0, rc, 1e-8);
                    break;
                case Status.AtUpper:
                    var ub = solver.ColumnUpper[j];
                    Assert.AreEqual(0.0, ub-xj, 1e-8);
                    break;
                case Status.AtLower:
                    var lb = solver.ColumnLower[j];
                    Assert.AreEqual(0.0, lb-xj, 1e-8);
                    break;
                }
            }
        }
    }
}
