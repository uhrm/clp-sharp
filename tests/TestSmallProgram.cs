using System;
using NUnit.Framework;

namespace Clp.Tests
{
    //   Minimize:
    // 
    //             x1 + 4 * x2 + 9 * x3
    // 
    //   Subject to:
    // 
    //             x1     + x2          <=  5
    //       10 <= x1              + x3
    //        7 <=        - x2     + x3 <=  7
    // 
    //   With:
    // 
    //     0 <= x1 <= 4
    //    -1 <= x2 <= 1
    //          x3 unrestricted.

    [TestFixture]
    public class TestSmallProgram : TestLp
    {
        private const double infty = Double.PositiveInfinity;

        private Clp.Solver CreateSolver()
        {
            var mod = new Clp.Solver();
            mod.LoadProblem(3, 3,
                new int[] { 0, 2, 4, 6 },
                new int[] { 0, 1, 0, 2, 1, 2 },
                new double[] { 1.0, 1.0, 1.0, -1.0, 1.0, 1.0 },
                new double[] { 0.0, -1.0, -infty },
                new double[] { 4.0, 1.0, infty },
                new double[] { 1.0, 4.0, 9.0 },
                new double[] { -infty, 10.0, 7.0 },
                new double[] { 5.0, infty, 7.0 });
            mod.LogLevel = 0;
            return mod;
        }

        [Test]
        public void TestCreate()
        {
            using (var mod = CreateSolver())
            {
                Assert.AreEqual(3, mod.NumRows);
                Assert.AreEqual(3, mod.NumColumns);

                // objective
                Assert.AreEqual(1.0, mod.Objective[0], 1e-14);
                Assert.AreEqual(4.0, mod.Objective[1], 1e-14);
                Assert.AreEqual(9.0, mod.Objective[2], 1e-14);

                // row lower bounds
                Assert.IsTrue(mod.RowLower[0] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 0, mod.RowLower[0]));
                Assert.AreEqual(10.0, mod.RowLower[1], 1e-14);
                Assert.AreEqual( 7.0, mod.RowLower[2], 1e-14);

                // row upper bounds
                Assert.AreEqual(5.0, mod.RowUpper[0], 1e-14);
                Assert.IsTrue(mod.RowUpper[1] > 1e20, String.Format("Invalid upper bound of row {0}: expected value > 1e20, found {1}.", 1, mod.RowUpper[1]));
                Assert.AreEqual(7.0, mod.RowUpper[2], 1e-14);

                // column lower bounds
                Assert.AreEqual( 0.0, mod.ColumnLower[0], 1e-14);
                Assert.AreEqual(-1.0, mod.ColumnLower[1], 1e-14);
                Assert.IsTrue(mod.ColumnLower[2] < -1e20, String.Format("Invalid lower bound of row {0}: expected value < -1e20, found {1}.", 2, mod.ColumnLower[2]));

                // column upper bounds
                Assert.AreEqual( 4.0, mod.ColumnUpper[0], 1e-14);
                Assert.AreEqual( 1.0, mod.ColumnUpper[1], 1e-14);
                Assert.IsTrue(mod.ColumnUpper[2] > 1e20, String.Format("Invalid upper bound of row {0}: expected value > 1e20, found {1}.", 2, mod.ColumnUpper[2]));

                // constraint matrix
                Assert.AreEqual(6, mod.NumElements);
                var Ap = new int[3];
                var Al = new int[3];
                var Ai = new int[6];
                var Ax = new double[6];
                mod.VectorStarts.CopyTo(Ap);
                mod.VectorLengths.CopyTo(Al);
                mod.Indices.CopyTo(Ai);
                mod.Elements.CopyTo(Ax);
                var v = new double[] { 1.0, 1.0, 0.0, 1.0, 0.0, -1.0, 0.0, 1.0, 1.0 };
                for (var j=0; j<3; j++) {
                    for (var k=Ap[j]; k<Ap[j]+Al[j]; k++) {
                        var i = Ai[k];
                        var x = Ax[k];
                        Assert.AreEqual(v[3*j+i], x, 1e-14, String.Format("Invalid matrix element at ({0},{1}): expected {2}, found {3}.", i, j, v[2*j+i], x));
                    }
                }
            }
        }
        
        [Test]
        public void TestSolve()
        {
            using (var mod = CreateSolver())
            {
                mod.InitialSolve();

                Assert.AreEqual(mod.Status, ProblemStatus.Optimal);

                Assert.AreEqual(54.0, mod.ObjectiveValue, 1e-12);

                Assert.AreEqual( 4.0, mod.PrimalColumnSolution[0], 1e-12);
                Assert.AreEqual(-1.0, mod.PrimalColumnSolution[1], 1e-12);
                Assert.AreEqual( 6.0, mod.PrimalColumnSolution[2], 1e-12);

                CheckKKT(mod, 1e-12);
            }
        }
    }
}
