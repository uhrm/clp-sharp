﻿using System;
using NUnit.Framework;

namespace Clp.Tests
{
    [TestFixture]
    public class TestAddRowCol
    {

        [Test]
        public void TestSimple()
        {
            var m = 1;
            var n = 10;

            using (var mod = new Clp.Solver())
            {
                mod.LogLevel = 0;

                mod.Resize(2*n+m, 0);

                // over-production constraints
                for (var j=0; j<n; j++) {
                    mod.RowLower[j] = -0.1;
                    mod.RowUpper[j] = Double.PositiveInfinity;
                    mod.RowName[j] = $"O{j:0000}";
                }
                // under-production constraints
                for (var j=0; j<n; j++) {
                    mod.RowLower[n+j] = 0.1;
                    mod.RowUpper[n+j] = Double.PositiveInfinity;
                    mod.RowName[n+j] = $"U{j:0000}";
                }
                // convexity constraint
                mod.RowLower[2*n] = 1.0;
                mod.RowUpper[2*n] = 1.0;
                mod.RowName[2*n] = $"CVX";
                // epigraph variables
                var zidx = new int[2];
                var zval = new double[2];
                for (var j=0; j<n; j++) {
                    zidx[0] = j;
                    zval[0] = 1.0;
                    zidx[1] = n+j;
                    zval[1] = 1.0;
                    mod.AddColumn(Double.NegativeInfinity, Double.PositiveInfinity, 1.0, zidx, zval);
                    mod.ColumnName[j] = $"Z{j:0000}";
                }
                
                // mod.WriteMps("TST_DBG_0.mps");

                // cut
                mod.AddRow(0.5, Double.PositiveInfinity,
                    new int[]    {   3,   5,   7 },
                    new double[] { 1.0, 1.0, 1.0 });
                mod.RowName[2*n+m] = "CUT";
                
                // mod.WriteMps("TST_DBG_1.mps");

                // column
                mod.AddColumn(0.0, Double.PositiveInfinity, 0.0,
                    new int[]    {    0,    1,    2,    3,    4,    5,    6,    7,    8,    9,
                                     10,   11,   12,   13,   14,   15,   16,   17,   18,   19,
                                     20 },
                    new double[] { -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
                                    1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,
                                    1.0 });
                mod.ColumnName[n] = "COL";
                
                // mod.WriteMps("TST_DBG_2.mps");

                mod.InitialSolve();

                // Console.WriteLine($"*** model status: {mod.Status}");
                // Console.WriteLine($"*** solution:");
                // for (var j=0; j<n+1; j++) {
                //     Console.WriteLine($"***   {mod.ColumnName[j]}: {mod.PrimalColumnSolution[j]:0.000}");
                // }
                // Console.WriteLine($"*** objective value: {mod.ObjectiveValue:0.000}");
                Assert.AreEqual(ProblemStatus.Optimal, mod.Status);
                Assert.AreEqual(9.0, mod.ObjectiveValue, 1e-14);
			}
        }
    }
}
