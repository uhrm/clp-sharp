
//
//  Solver.cs
//  clp-sharp
//
//  Copyright 2015-2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Collections;
using System.Collections.Generic;

namespace Clp;

public sealed class ClpException : Exception
{
    public ClpException(string message) : base(message) { }
}

public enum ProblemStatus : int
{
    /** unknown e.g. before solve or if post-solve says not optimal */
    Unknown          = -1,
    /** optimal */
    Optimal          =  0,
    /** primal infeasible */
    PrimalInfeasible =  1,
    /** dual infeasible */
    DualInfeasible   =  2,
    /** stopped on iterations or time */
    IterationsOrTime =  3,
    /** stopped due to errors */
    Errors           =  4,
    /** stopped by event handler (virtual int ClpEventHandler::event()) */
    EventHandler     =  5
}

public enum SecondaryStatus : int
{
    None = 0,
    DualLimitReached = 1,
    UnscaledPrimalInfeasibilities = 2,
    UnscaledDualInfeasibilities = 3,
    UnscaledPrimalAndDualInfeasibilities = 4,
    PrimalWithFlaggedVariables = 5,
    EmptyProblem = 6,
    PostSolveNotOptimal = 7,
    BadElementCheck = 8,
    StoppedOnTime = 9,
    StoppedAsPrimalFeasible = 10,
    // CLP event codes
    EndOfIteration = 100, // used to set secondary status
    EndOfFactorization = 101, // after gutsOfSolution etc
    EndOfValuesPass = 102,
    Node = 103, // for Cbc
    TreeStatus = 104, // for Cbc
    Solution = 105, // for Cbc
    Theta = 106, // hit in parametrics
    PivotRow = 107, // used to choose pivot row
    PresolveStart = 108, // ClpSolve presolve start
    PresolveSize = 109, // sees if ClpSolve presolve too big or too small
    PresolveInfeasible = 110, // ClpSolve presolve infeasible
    PresolveBeforeSolve = 111, // ClpSolve presolve before solve
    PresolveAfterFirstSolve = 112, // ClpSolve presolve after solve
    PresolveAfterSolve = 113, // ClpSolve presolve after solve
    PresolveEnd = 114, // ClpSolve presolve end
    GoodFactorization = 115, // before gutsOfSolution
    ComplicatedPivotIn = 116, // in modifyCoefficients
    NoCandidateInPrimal = 117, // tentative end
    LooksEndInPrimal = 118, // About to declare victory (or defeat)
    EndInPrimal = 119, // Victory (or defeat)
    BeforeStatusOfProblemInPrimal = 120,
    StartOfStatusOfProblemInPrimal = 121,
    ComplicatedPivotOut = 122, // in modifyCoefficients
    NoCandidateInDual = 123, // tentative end
    LooksEndInDual = 124, // About to declare victory (or defeat)
    EndInDual = 125, // Victory (or defeat)
    BeforeStatusOfProblemInDual = 126,
    StartOfStatusOfProblemInDual = 127,
    StartOfIterationInDual = 128,
    UpdateDualsInDual = 129,
    BeforeDeleteRim = 130,
    EndOfCreateRim = 131,
    SlightlyInfeasible = 132,
    ModifyMatrixInMiniPresolve = 133,
    MoreMiniPresolve = 134,
    ModifyMatrixInMiniPostsolve = 135,
    BeforeChooseIncoming = 136,
    AfterChooseIncoming = 137,
    BeforeCreateNonLinear = 138,
    AfterCreateNonLinear = 139,
    StartOfCrossover = 140, // in Idiot
    NoTheta = 141 // At end (because no pivot)
}

public enum Status : byte
{
    Free       = 0x00,
    Basic      = 0x01,
    AtUpper    = 0x02,
    AtLower    = 0x03,
    SuperBasic = 0x04,
    Fixed      = 0x05
}

public sealed class Solver : IDisposable
{
    // generic error checking

    private static void CheckIndex(int i, int n, string name)
    {
        if (i < 0 || i >= n) {
            throw new ArgumentException($"Invalid element index (expected 0 <= {name} < {n}, found {name} = {i})", name);
        }
    }

    private static void CheckSpanLen<T>(ReadOnlySpan<T> array, int n, string name)
    {
        if (array.Length != n) {
            throw new ArgumentException($"Invalid length of \"{name}\" array (expected len = {n}, found len = {array.Length})", name);
        }
    }

    internal delegate int ClpPropertyCount(IntPtr model);

    internal delegate string ClpStrPropertyGetter(IntPtr model, int index);

    internal delegate void ClpStrPropertySetter(IntPtr model, int index, string value);

    public class RwStrVecProperty : IReadOnlyList<string>
    {
        private readonly Solver model;
        private readonly ClpPropertyCount _count;
        private readonly ClpStrPropertyGetter _get;
        private readonly ClpStrPropertySetter _set;
        internal RwStrVecProperty(Solver model, ClpPropertyCount c, ClpStrPropertyGetter g, ClpStrPropertySetter s)
        {
            this.model = model;
            this._count = c;
            this._get = g;
            this._set = s;
            var n = c(model.clp_solver);
            // Note: CLP does not support reading 'null' strings
            for (var i=0; i<n; i++) {
                s(model.clp_solver, i, "");
            }
        }
        public string this[int i]
        {
            get {
                CheckIndex(i, this._count(this.model.clp_solver), nameof(i));
                return this._get(this.model.clp_solver, i);
            }
            set {
                CheckIndex(i, this._count(this.model.clp_solver), nameof(i));
                this._set(this.model.clp_solver, i, value);
            }
        }
        public int Count
        {
            get { return this._count(this.model.clp_solver); }
        }
        public IEnumerator<string> GetEnumerator()
        {
            var n = this._count(this.model.clp_solver);
            for (var i=0; i<n; i++) {
                yield return this._get(this.model.clp_solver, i);
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    private nint clp_solver;

    public Solver()
    {
        this.clp_solver = NativeMethods.Clp_newModel();
        if (this.clp_solver == IntPtr.Zero) {
            throw new ClpException("Error creating Clp model.");
        }
    }

    ~Solver()
    {
        Dispose(false);
    }

    // IDisposable API

    private bool disposed = false;

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        if (!disposed) {
            NativeMethods.Clp_deleteModel(clp_solver);
            clp_solver = IntPtr.Zero;
        }
        disposed = true;
    }

    //
    // Solver API
    //

    // problem creation/modification

    public void LoadProblem(int numcols, int numrows,
        ReadOnlySpan<int> start, ReadOnlySpan<int> index, ReadOnlySpan<double> value,
        ReadOnlySpan<double> collb, ReadOnlySpan<double> colub, ReadOnlySpan<double> obj,
        ReadOnlySpan<double> rowlb, ReadOnlySpan<double> rowub)
    {
        CheckSpanLen(start, numcols+1, nameof(start));
        int nnz = start[numcols];
        CheckSpanLen(index, nnz, nameof(index));
        CheckSpanLen(value, nnz, nameof(value));
        CheckSpanLen(collb, numcols, nameof(collb));
        CheckSpanLen(colub, numcols, nameof(colub));
        CheckSpanLen(obj, numcols, nameof(obj));
        CheckSpanLen(rowlb, numrows, nameof(rowlb));
        CheckSpanLen(rowub, numrows, nameof(rowub));
        NativeMethods.Clp_loadProblem(this.clp_solver,
            numcols, numrows,
            in start.GetPinnableReference(),
            in index.GetPinnableReference(),
            in value.GetPinnableReference(),
            in collb.GetPinnableReference(),
            in colub.GetPinnableReference(),
            in obj.GetPinnableReference(),
            in rowlb.GetPinnableReference(),
            in rowub.GetPinnableReference());
    }

    public void Resize(int nrows, int ncols)
    {
        NativeMethods.Clp_resize(this.clp_solver, nrows, ncols);
    }

    public void DeleteRows(int index)
    {
        NativeMethods.Clp_deleteRows(this.clp_solver, 1, in index);
    }

    public void DeleteRows(ReadOnlySpan<int> index)
    {
        NativeMethods.Clp_deleteRows(this.clp_solver, index.Length, in index.GetPinnableReference());
    }

    public void AddRow(double rowLower, double rowUpper,
        ReadOnlySpan<int> columns, ReadOnlySpan<double> elements)
    {
        int nnz = columns.Length;
        CheckSpanLen(elements, nnz, nameof(elements));
        Span<int> rowStarts = stackalloc int[] { 0, nnz };
        NativeMethods.Clp_addRows(this.clp_solver, 1,
            in rowLower,
            in rowUpper,
            in rowStarts.GetPinnableReference(),
            in columns.GetPinnableReference(),
            in elements.GetPinnableReference());
    }

    public void AddRows(ReadOnlySpan<double> rowLower, ReadOnlySpan<double> rowUpper,
        ReadOnlySpan<int> rowStarts, ReadOnlySpan<int> columns, ReadOnlySpan<double> elements)
    {
        int n = rowLower.Length;
        CheckSpanLen(rowUpper, n, nameof(rowUpper));
        CheckSpanLen(rowStarts, n+1, nameof(rowStarts));
        int nnz = rowStarts[n];
        CheckSpanLen(columns, nnz, nameof(columns));
        CheckSpanLen(elements, nnz, nameof(elements));
        NativeMethods.Clp_addRows(this.clp_solver, n,
            in rowLower.GetPinnableReference(),
            in rowUpper.GetPinnableReference(),
            in rowStarts.GetPinnableReference(),
            in columns.GetPinnableReference(),
            in elements.GetPinnableReference());
    }

    public void DeleteColumn(int index)
    {
        NativeMethods.Clp_deleteColumns(this.clp_solver, 1, in index);
    }

    public void DeleteColumns(ReadOnlySpan<int> index)
    {
        NativeMethods.Clp_deleteColumns(this.clp_solver, index.Length, in index.GetPinnableReference());
    }

    public void AddColumn(
        double columnLower, double columnUpper, double objective,
        ReadOnlySpan<int> rows, ReadOnlySpan<double> elements)
    {
        int nnz = rows.Length;
        CheckSpanLen(elements, nnz, nameof(elements));
        Span<int> columnStarts = stackalloc int[] { 0, nnz };
        NativeMethods.Clp_addColumns(this.clp_solver, 1,
            in columnLower,
            in columnUpper,
            in objective,
            in columnStarts.GetPinnableReference(),
            in rows.GetPinnableReference(),
            in elements.GetPinnableReference());
    }

    public void AddColumns(
        ReadOnlySpan<double> columnLower, ReadOnlySpan<double> columnUpper, ReadOnlySpan<double> objective,
        ReadOnlySpan<int> columnStarts, ReadOnlySpan<int> rows, ReadOnlySpan<double> elements)
    {
        int n = columnLower.Length;
        CheckSpanLen(columnUpper, n, nameof(columnUpper));
        CheckSpanLen(objective, n, nameof(objective));
        CheckSpanLen(columnStarts, n+1, nameof(columnStarts));
        int nnz = columnStarts[n];
        CheckSpanLen(rows, nnz, nameof(rows));
        CheckSpanLen(elements, nnz, nameof(elements));
        NativeMethods.Clp_addColumns(this.clp_solver, n,
            in columnLower.GetPinnableReference(),
            in columnUpper.GetPinnableReference(),
            in objective.GetPinnableReference(),
            in columnStarts.GetPinnableReference(),
            in rows.GetPinnableReference(),
            in elements.GetPinnableReference());
    }

    // model I/O

    public void ReadMps(string filename)
    {
        NativeMethods.Clp_readMps(this.clp_solver, filename, 1, 0);
    }

    public void WriteMps(string filename)
    {
        NativeMethods.Clp_writeMps(this.clp_solver, filename, 0, 1, this.OptimizationDirection);
    }

    // problem data query methods

    public Span<double> RowLower
    {
        get {
            unsafe {
                return new Span<double>(
                    NativeMethods.Clp_rowLower(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumRows(this.clp_solver));
            }
        }
    }

    public Span<double> RowUpper
    {
        get {
            unsafe {
                return new Span<double>(
                    NativeMethods.Clp_rowUpper(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumRows(this.clp_solver));
            }
        }
    }

    public Span<double> Objective
    {
        get {
            unsafe {
                return new Span<double>(
                    NativeMethods.Clp_objective(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumCols(this.clp_solver));
            }
        }
    }

    public Span<double> ColumnLower
    {
        get {
            unsafe {
                return new Span<double>(
                    NativeMethods.Clp_columnLower(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumCols(this.clp_solver));
            }
        }
    }

    public Span<double> ColumnUpper
    {
        get {
            unsafe {
                return new Span<double>(
                    NativeMethods.Clp_columnUpper(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumCols(this.clp_solver));
            }
        }
    }

    public Span<int> VectorStarts
    {
        get {
            unsafe {
                return new Span<int>(
                    NativeMethods.Clp_getVectorStarts(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumCols(this.clp_solver));
            }
        }
    }

    public Span<int> VectorLengths
    {
        get {
            unsafe {
                return new Span<int>(
                    NativeMethods.Clp_getVectorLengths(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumCols(this.clp_solver));
            }
        }
    }

    public Span<int> Indices
    {
        get {
            var n = NumColumns;
            var len = VectorStarts[n-1] + VectorLengths[n-1];
            unsafe {
                return new Span<int>(
                    NativeMethods.Clp_getIndices(this.clp_solver).ToPointer(), len);
            }
        }
    }

    public Span<double> Elements
    {
        get {
            var n = NumColumns;
            var len = VectorStarts[n-1] + VectorLengths[n-1];
            unsafe {
                return new Span<double>(
                    NativeMethods.Clp_getElements(this.clp_solver).ToPointer(), len);
            }
        }
    }

    public void ChangeRowLower(ReadOnlySpan<double> rowLower)
    {
        int n = NumRows;
        CheckSpanLen(rowLower, n, nameof(rowLower));
        NativeMethods.Clp_chgRowLower(this.clp_solver, in rowLower.GetPinnableReference());
    }

    public void ChangeRowUpper(ReadOnlySpan<double> rowUpper)
    {
        int n = NumRows;
        CheckSpanLen(rowUpper, n, nameof(rowUpper));
        NativeMethods.Clp_chgRowUpper(this.clp_solver, in rowUpper.GetPinnableReference());
    }

    public void ChangeColumnLower(ReadOnlySpan<double> columnLower)
    {
        int n = this.NumColumns;
        CheckSpanLen(columnLower, n, nameof(columnLower));
        NativeMethods.Clp_chgColumnLower(this.clp_solver, in columnLower.GetPinnableReference());
    }

    public void ChangeColumnUpper(ReadOnlySpan<double> columnUpper)
    {
        int n = this.NumColumns;
        CheckSpanLen(columnUpper, n, nameof(columnUpper));
        NativeMethods.Clp_chgColumnUpper(this.clp_solver, in columnUpper.GetPinnableReference());
    }

    public void ChangeObjCoefficients(ReadOnlySpan<double> objCoefs)
    {
        int n = this.NumColumns;
        CheckSpanLen(objCoefs, n, nameof(objCoefs));
        NativeMethods.Clp_chgObjCoefficients(this.clp_solver, in objCoefs.GetPinnableReference());
    }

    // solution query methods

    public ReadOnlySpan<double> PrimalRowSolution
    {
        get {
            unsafe {
                return new ReadOnlySpan<double>(
                    NativeMethods.Clp_primalRowSolution(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumRows(this.clp_solver));
            }
        }
    }

    public ReadOnlySpan<double> PrimalColumnSolution
    {
        get {
            unsafe {
                return new ReadOnlySpan<double>(
                    NativeMethods.Clp_primalColumnSolution(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumCols(this.clp_solver));
            }
        }
    }

    public ReadOnlySpan<double> DualRowSolution
    {
        get {
            unsafe {
                return new ReadOnlySpan<double>(
                    NativeMethods.Clp_dualRowSolution(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumRows(this.clp_solver));
            }
        }
    }

    public ReadOnlySpan<double> DualColumnSolution
    {
        get {
            unsafe {
                return new ReadOnlySpan<double>(
                    NativeMethods.Clp_dualColumnSolution(this.clp_solver).ToPointer(),
                    NativeMethods.Clp_getNumCols(this.clp_solver));
            }
        }
    }

    public void GetInfeasibilityRay(Span<double> ray)
    {
        var m = NativeMethods.Clp_getNumRows(this.clp_solver);
        var ptr = NativeMethods.Clp_infeasibilityRay(this.clp_solver);
        if (ptr == IntPtr.Zero) {
            throw new ClpException("Infeasibility ray is null;");
        }
        try {
            unsafe {
                new ReadOnlySpan<double>(ptr.ToPointer(), m).CopyTo(ray);
            }
        }
        finally {
            NativeMethods.Clp_freeRay(this.clp_solver, ptr);
        }
    }

    public void GetUnboundedRay(Span<double> ray)
    {
        var n = NativeMethods.Clp_getNumCols(this.clp_solver);
        var ptr = NativeMethods.Clp_unboundedRay(this.clp_solver);
        if (ptr == IntPtr.Zero) {
            throw new ClpException("Unbounded ray is null;");
        }
        try {
            unsafe {
                new ReadOnlySpan<double>(ptr.ToPointer(), n).CopyTo(ray);
            }
        }
        finally {
            NativeMethods.Clp_freeRay(this.clp_solver, ptr);
        }
    }


    // solver methods

    public int InitialSolve()
    {
        var result = NativeMethods.Clp_initialSolve(this.clp_solver);
        return result;
    }

//        public int InitialSolveWithOptions(Options options)
//        {
//        }

    public int InitialDualSolve()
    {
        var result = NativeMethods.Clp_initialDualSolve(this.clp_solver);
        return result;
    }

    public int InitialPrimalSolve()
    {
        var result = NativeMethods.Clp_initialPrimalSolve(this.clp_solver);
        return result;
    }

    public int InitialBarrierSolve()
    {
        var result = NativeMethods.Clp_initialBarrierSolve(this.clp_solver);
        return result;
    }

    public int InitialBarrierNoCrossSolve()
    {
        var result = NativeMethods.Clp_initialBarrierNoCrossSolve(this.clp_solver);
        return result;
    }

    public int Dual(int mode = 0)
    {
        var result = NativeMethods.Clp_dual(this.clp_solver, mode);
        return result;
    }

    public int Primal(int mode = 0)
    {
        var result = NativeMethods.Clp_primal(this.clp_solver, mode);
        return result;
    }

    public void Idiot(int tryhard)
    {
        NativeMethods.Clp_idiot(this.clp_solver, tryhard);
    }

    public int Crash(double gap, int pivot)
    {
        var result = NativeMethods.Clp_crash(this.clp_solver, gap, pivot);
        return result;
    }

    // properties

    public int NumRows
    {
        get { return NativeMethods.Clp_getNumRows(this.clp_solver); }
    }

    public int NumColumns
    {
        get { return NativeMethods.Clp_getNumCols(this.clp_solver); }
    }

    public int NumElements
    {
        get { return NativeMethods.Clp_getNumElements(this.clp_solver); }
    }

    public double PrimalTolerance
    {
        get { return NativeMethods.Clp_primalTolerance(this.clp_solver); }
        set { NativeMethods.Clp_setPrimalTolerance(this.clp_solver, value); }
    }

    public double DualTolerance
    {
        get { return NativeMethods.Clp_dualTolerance(this.clp_solver); }
        set { NativeMethods.Clp_setDualTolerance(this.clp_solver, value); }
    }

    public double DualObjectiveLimit
    {
        get { return NativeMethods.Clp_dualObjectiveLimit(this.clp_solver); }
        set { NativeMethods.Clp_setDualObjectiveLimit(this.clp_solver, value); }
    }

    public double ObjectiveOffset
    {
        get { return NativeMethods.Clp_objectiveOffset(this.clp_solver); }
        set { NativeMethods.Clp_setObjectiveOffset(this.clp_solver, value); }
    }

    public string ProblemName
    {
        get { return NativeMethods.Clp_problemName(this.clp_solver, 255); }
        set { NativeMethods.Clp_setProblemName(this.clp_solver, value.Length, value); }
    }

    public int NumIterations
    {
        get { return NativeMethods.Clp_numberIterations(this.clp_solver); }
        set { NativeMethods.Clp_setNumberIterations(this.clp_solver, value); }
    }

    public int MaximumIterations
    {
        get { return NativeMethods.maximumIterations(this.clp_solver); }
        set { NativeMethods.Clp_setMaximumIterations(this.clp_solver, value); }
    }

    public double MaximumSeconds
    {
        get { return NativeMethods.Clp_maximumSeconds(this.clp_solver); }
        set { NativeMethods.Clp_setMaximumSeconds(this.clp_solver, value); }
    }

    public bool HitMaximumIterations
    {
        get { return NativeMethods.Clp_hitMaximumIterations(this.clp_solver) != 0; }
    }

    public ProblemStatus Status
    {
        get { return (ProblemStatus)NativeMethods.Clp_status(this.clp_solver); }
        set { NativeMethods.Clp_setProblemStatus(this.clp_solver, (int)value); }
    }

    public SecondaryStatus SecondaryStatus
    {
        get { return (SecondaryStatus)NativeMethods.Clp_secondaryStatus(this.clp_solver); }
        set { NativeMethods.Clp_setSecondaryStatus(this.clp_solver, (int)value); }
    }

    public double OptimizationDirection
    {
        get { return NativeMethods.Clp_optimizationDirection(this.clp_solver); }
        set { NativeMethods.Clp_setOptimizationDirection(this.clp_solver, value); }
    }

    public int LogLevel
    {
        get { return NativeMethods.Clp_logLevel(this.clp_solver); }
        set { NativeMethods.Clp_setLogLevel(this.clp_solver, value); }
    }

    public int ScalingFlag
    {
        get { return NativeMethods.Clp_scalingFlag(this.clp_solver); }
        set { NativeMethods.Clp_scaling(this.clp_solver, value); }
    }

    public double ObjectiveValue
    {
        get { return NativeMethods.Clp_objectiveValue(this.clp_solver); }
    }

    // status array indexer

    public ref struct StatusSpan
    {
        private readonly ReadOnlySpan<byte> span;
        internal StatusSpan(IntPtr ptr, int len, int ofs)
        {
            unsafe {
                span = new ReadOnlySpan<byte>(ptr.ToPointer(), len).Slice(ofs);
            }
        }
        public readonly Status this[int i]
        {
            get
            {
                // Note: Clp leaks an implementation detail by exposing the internal
                // status array; only the three least significant bits encode the
                // actual status
                return (Status)(span[i] & 7);
            }
        }
    }

    public StatusSpan RowStatus
    {
        get {
            var m = NativeMethods.Clp_getNumRows(this.clp_solver);
            var n = NativeMethods.Clp_getNumCols(this.clp_solver);
            return new StatusSpan(NativeMethods.Clp_statusArray(this.clp_solver), m + n, n);
        }
    }

    public StatusSpan ColumnStatus
    {
        get {
            var m = NativeMethods.Clp_getNumRows(this.clp_solver);
            var n = NativeMethods.Clp_getNumCols(this.clp_solver);
            return new StatusSpan(NativeMethods.Clp_statusArray(this.clp_solver), n, 0);
        }
    }

    private RwStrVecProperty? _rowName;
    public RwStrVecProperty RowName => this._rowName ??=
        new RwStrVecProperty(this,
            new ClpPropertyCount(NativeMethods.Clp_getNumRows),
            new ClpStrPropertyGetter(NativeMethods.Clp_rowName),
            new ClpStrPropertySetter(NativeMethods.Clp_setRowName));

    private RwStrVecProperty? _columnName;
    public RwStrVecProperty ColumnName => this._columnName ??=
        new RwStrVecProperty(
            this,
            new ClpPropertyCount(NativeMethods.Clp_getNumCols),
            new ClpStrPropertyGetter(NativeMethods.Clp_columnName),
            new ClpStrPropertySetter(NativeMethods.Clp_setColumnName));

    // solver data copy

    public static void Copy(Solver src, Solver dst)
    {
        dst.ProblemName = src.ProblemName;
        var m = src.NumRows;
        var n = src.NumColumns;
        dst.Resize(m, 0);
        // copy row bounds
        for (var i=0; i<m; i++) {
            dst.RowLower[i] = src.RowLower[i];
            dst.RowUpper[i] = src.RowUpper[i];
        }
        // copy constraint matrix (by column)
        for (var j=0; j<n; j++) {
            var jbeg = src.VectorStarts[j];
            var jlen = src.VectorLengths[j];
            var lb = src.ColumnLower[j];
            var ub = src.ColumnUpper[j];
            var obj = src.Objective[j];
            dst.AddColumn(lb, ub, obj, src.Indices.Slice(jbeg, jlen), src.Elements.Slice(jbeg, jlen));
        }
        // copy row/column name
        for (var i=0; i<m; i++) {
            dst.RowName[i] = src.RowName[i];
        }
        for (var i=0; i<n; i++) {
            dst.ColumnName[i] = src.ColumnName[i];
        }
    }

    // solver metadata

    public static (int major, int minor, int release) Version
    {
        get {
            return (
                NativeMethods.Clp_VersionMajor(),
                NativeMethods.Clp_VersionMinor(),
                NativeMethods.Clp_VersionRelease()
            );
        }
    }
}
