
//  
//  NativeMethods.cs
//  clp-sharp
//
//  Copyright 2015-2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Runtime.InteropServices;

namespace Clp;

internal partial class NativeMethods
{

    // native functions

    [DllImport("CoinUtils", EntryPoint="clock")]
    public static extern IntPtr load_coinutils();

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_VersionMajor();

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_VersionMinor();

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_VersionRelease();

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_newModel();

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_deleteModel(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_loadProblem(IntPtr model, int numcols, int numrows,
        in int start, in int index, in double value,
        in double collb, in double colub, in double obj,
        in double rowlb, in double rowub);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_resize(IntPtr model, int newNumberRows, int newNumberColumns);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_deleteRows(IntPtr model, int number, in int index);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_addRows(IntPtr model, int number, in double rowLower, in double rowUpper,
        in int rowStarts, in int columns, in double elements);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_deleteColumns(IntPtr model, int number, in int index);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_addColumns(IntPtr model, int number, in double columnLower, in double columnUpper,
        in double objective, in int columnStarts, in int rows, in double elements);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_chgRowLower(IntPtr model, in double rowLower);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_chgRowUpper(IntPtr model, in double rowUpper);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_chgColumnLower(IntPtr model, in double columnLower);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_chgColumnUpper(IntPtr model, in double columnUpper);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_chgObjCoefficients(IntPtr model, in double objIn);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_getNumRows(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_getNumCols(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_getNumElements(IntPtr model);

//        [DllImport("Clp")]
//        public static extern int Clp_numberRows(IntPtr model);

//        [DllImport("Clp")]
//        public static extern int Clp_numberColumns(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial double Clp_primalTolerance(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setPrimalTolerance(IntPtr model, double value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial double Clp_dualTolerance(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setDualTolerance(IntPtr model, double value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial double Clp_dualObjectiveLimit(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setDualObjectiveLimit(IntPtr model, double value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial double Clp_objectiveOffset(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setObjectiveOffset(IntPtr model, double value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    private static partial void Clp_problemName(IntPtr model, int maxNumberCharacters, Span<byte> array);

    internal static string Clp_problemName(IntPtr model, int maxNumberCharacters)
    {
        // see also https://github.com/dotnet/roslyn/issues/25118
        Span<byte> buf = maxNumberCharacters < 1024
            ? stackalloc byte[maxNumberCharacters+1]
            : new byte[maxNumberCharacters+1];
        Clp_problemName(model, maxNumberCharacters, buf);
        return System.Text.Encoding.Default.GetString(buf.Slice(0, buf.IndexOf((byte)0)));
    }

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_setProblemName(IntPtr model, int maxNumberCharacters, string array);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_numberIterations(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setNumberIterations(IntPtr model, int numberIterations);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int maximumIterations(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setMaximumIterations(IntPtr model, int value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial double Clp_maximumSeconds(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setMaximumSeconds(IntPtr model, double value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_hitMaximumIterations(IntPtr model);

    /**
        * Status of problem:
        *  0 - optimal
        *  1 - primal infeasible
        *  2 - dual infeasible
        *  3 - stopped on iterations etc
        *  4 - stopped due to errors
        */
    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_status(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setProblemStatus(IntPtr model, int problemStatus);

    /**
        * Secondary status of problem - may get extended
        *  0 - none
        *  1 - primal infeasible because dual limit reached
        *  2 - scaled problem optimal - unscaled has primal infeasibilities
        *  3 - scaled problem optimal - unscaled has dual infeasibilities
        *  4 - scaled problem optimal - unscaled has both dual and primal infeasibilities
        */
    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_secondaryStatus(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setSecondaryStatus(IntPtr model, int status);

    /**
        * Direction of optimization
        *  1 - minimize
        * -1 - maximize
        *  0 - ignore
        */
    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial double Clp_optimizationDirection(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setOptimizationDirection(IntPtr model, double value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_primalRowSolution(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_primalColumnSolution(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_dualRowSolution(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_dualColumnSolution(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_rowLower(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_rowUpper(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_objective(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_columnLower(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_columnUpper(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_getVectorStarts(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_getIndices(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_getVectorLengths(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_getElements(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_initialSolve(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_initialSolveWithOptions(IntPtr model, IntPtr options);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_initialDualSolve(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_initialPrimalSolve(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_initialBarrierSolve(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_initialBarrierNoCrossSolve(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_dual(IntPtr model, int ifValuesPass);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_primal(IntPtr model, int ifValuesPass);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_idiot(IntPtr model, int tryhard);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_scaling(IntPtr model, int mode);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_scalingFlag(IntPtr model);

    /**
        * Crash - at present just aimed at dual. Returns
        * -2 - if dual preferred and crash basis created
        * -1 - if dual preferred and all slack basis preferred
        *  0 - if basis going in was not all slack
        *  1 - if primal preferred and all slack basis preferred
        *  2 - if primal preferred and crash basis created.
        * 
        * If gap between bounds <= "gap" variables can be flipped
        * 
        * If "pivot" is
        *  0 - No pivoting (so will just be choice of algorithm)
        *  1 - Simple pivoting e.g. gub
        *  2 - Mini iterations
        */
    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_crash(IntPtr model, double gap, int pivot);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial double Clp_objectiveValue(IntPtr model);

    /**
        * Status values
        *   0 - free
        *   1 - basic
        *   2 - at upper
        *   3 - at lower
        *   4 - superbasic
        *  (5 - fixed)
        */
    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_statusArray(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_getColumnStatus(IntPtr model, int index);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_getRowStatus(IntPtr model, int index);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setColumnStatus(IntPtr model, int index, int value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setRowStatus(IntPtr model, int index, int value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_infeasibilityRay(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial IntPtr Clp_unboundedRay(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_freeRay(IntPtr model, IntPtr ray);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_primalFeasible(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_logLevel(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setLogLevel(IntPtr model, int value);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial int Clp_lengthNames(IntPtr model);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    private static partial void Clp_columnName(IntPtr model, int idx, Span<byte> buf);

    internal static string Clp_columnName(IntPtr model, int idx)
    {
        var len = Clp_lengthNames(model);
        // see also https://github.com/dotnet/roslyn/issues/25118
        Span<byte> buf = len < 1024
            ? stackalloc byte[len+1]
            : new byte[len+1];
        Clp_columnName(model, idx, buf);
        return System.Text.Encoding.Default.GetString(buf.Slice(0, buf.IndexOf((byte)0)));
    }

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setColumnName(IntPtr model, int idx, string name); // Note: passing a NULL string to Clp_setColumnName results in a C++ runtime error

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    private static partial void Clp_rowName(IntPtr model, int idx, Span<byte> buf);

    public static string Clp_rowName(IntPtr model, int idx)
    {
        var len = Clp_lengthNames(model);
        // see also https://github.com/dotnet/roslyn/issues/25118
        Span<byte> buf = len < 1024
            ? stackalloc byte[len+1]
            : new byte[len+1];
        Clp_rowName(model, idx, buf);
        // Clp_rowName(model, idx, in buf.GetPinnableReference());
        return System.Text.Encoding.Default.GetString(buf.Slice(0, buf.IndexOf((byte)0)));
    }

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_setRowName(IntPtr model, int idx, string name); // Note: passing a NULL string to Clp_setRowName results in a C++ runtime error

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_readMps(IntPtr model, string filename, int keepNames, int ignoreErrors);

    [LibraryImport("Clp", StringMarshalling = StringMarshalling.Utf8)]
    internal static partial void Clp_writeMps(IntPtr model, string filename, int formatType, int numberAccross, double objSense);

}
